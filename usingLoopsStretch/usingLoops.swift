//
//  usingLoops.swift
//  usingLoopsStretch
//
//  Created by Taylor Petersen on 10/14/15.
//  Copyright © 2015 com.example. All rights reserved.
//

import UIKit

let x = 15
let y = 100

func everyThird(x:Int, y:Int) {
    for val : Int in 0..<y {
        if val % x == 2 {
            print(val)
        }
    }
    everyThird(x, y: y)
}


/* let x = 15
let y = 100


func name (x: Int, y: Int) {
    
    for i in 1...y {
        if (i % x == 0) {
            print(i)
        }
    }
    
/**/*/