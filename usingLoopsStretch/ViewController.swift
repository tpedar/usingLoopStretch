//
//  ViewController.swift
//  usingLoopsStretch
//
//  Created by Taylor Petersen on 10/14/15.
//  Copyright © 2015 com.example. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let x = 15
        let y = 100
        
        func everyThird(x:Int, y:Int) {
            for val : Int in 0..<y {
                if val % x == 0 {
                    print("\(val)")
                }
            }
           
        }
            everyThird(x, y: y)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // This is Develop
}

